'use strict';

/**
 * @ngdoc function
 * @name simpleAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the simpleAppApp
 */
angular.module('simpleAppApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

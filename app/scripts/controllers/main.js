'use strict';

/**
 * @ngdoc function
 * @name simpleAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the simpleAppApp
 */
angular.module('simpleAppApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
